<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Profile Vidya Dwi Turisqi Wijaya">
    <meta name="author" content="Vidya Dwi Turisqi Wijaya">
    <meta name="keywords" content="Vidya, Vidi, Wijaya, Vidya Dwi Turisqi Wijaya">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
    <title>Vidi Wijaya</title>
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullpage.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/templatemo-style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
</head>

<body>
    <div id="video">
        <div class="preloader">
            <div class="preloader-bounce">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <header id="header">
            <div class="container-fluid">
                <div class="navbar">
                    <a href="#" id="logo" title="Elegance by TemplateMo">
                        Portfolio
                    </a>
                    <div class="navigation-row">
                        <nav id="navigation">
                            <button type="button" class="navbar-toggle"> <i class="fa fa-bars"></i> </button>
                            <div class="nav-box navbar-collapse">
                                <ul class="navigation-menu nav navbar-nav navbars" id="nav">
                                    <li data-menuanchor="slide01" class="active"><a href="#slide01">Home</a></li>
                                    <li data-menuanchor="slide02"><a href="#slide02">About Me</a></li>
                                    <li data-menuanchor="slide03"><a href="#slide03">Work Experience</a></li>
                                    <li data-menuanchor="slide04"><a href="#slide04">Skills</a></li>
                                    <li data-menuanchor="slide05"><a href="#slide05">Project</a></li>
                                    <li data-menuanchor="slide06"><a href="#slide06">Contact Me</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <video autoplay muted loop id="myVideo">
            <source src="{{ asset('assets/images/video-bg.mp4') }}" type="video/mp4">
        </video>
        <div id="fullpage" class="fullpage-default">
            <div class="section animated-row" data-section="slide01">
                <div class="section-inner">
                    <div class="welcome-box">
                        <span class="welcome-first animate" data-animate="fadeInUp">Hi, my name is</span>
                        <h1 class="welcome-title animate" data-animate="fadeInUp">Vidi Wijaya</h1>
                        <p class="animate" data-animate="fadeInUp">Welcome to my portfolio website.</p>
                        <div class="scroll-down next-section animate data-animate= fadeInUp"><img src="{{ asset('assets/images/mouse-scroll.png') }}" alt=""><span>Scroll Down</span></div>
                    </div>
                </div>
            </div>
            <div class="section animated-row" data-section="slide02">
                <div class="section-inner">
                    <div class="about-section">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 wide-col-laptop">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="about-contentbox">
                                            <div class="animate" data-animate="fadeInUp">
                                                <span>About Me</span>
                                                <p>
                                                    My full name is Vidya Dwi Turisqi Wijaya, usually my friends call me Vidi.
                                                    I have an interest in technology, especially website programming.
                                                    I graduated from Bachelor of Informatics Engineering in 2014 at Dian Nuswantoro University in Semarang, Central Java.
                                                    Currently I work in Jakarta and I live in Depok, West Java.
                                                </p>
                                            </div>
                                            <div class="facts-list owl-carousel">
                                                <div class="item animate" data-animate="fadeInUp">
                                                    <div class="counter-box">
                                                        <i class="fa fa-code counter-icon" aria-hidden="true"></i><span class="count-number">5</span>Working Years
                                                    </div>
                                                </div>
                                                <div class="item animate" data-animate="fadeInUp">
                                                    <div class="counter-box">
                                                        <i class="fa fa-desktop counter-icon" aria-hidden="true"></i><span class="count-number">7</span></span> Development
                                                    </div>
                                                </div>
                                                <div class="item animate" data-animate="fadeInUp">
                                                    <div class="counter-box">
                                                        <i class="fa fa-desktop counter-icon" aria-hidden="true"></i><span class="count-number">4</span> Companies
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <figure class="about-img animate" data-animate="fadeInUp"><img src="{{ asset('assets/images/profile.jpg') }}" class="rounded" alt=""></figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section animated-row" data-section="slide03">
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-8 wide-col-laptop">
                            <div class="title-block animate" data-animate="fadeInUp">
                                <span>My Work Experiences</span>
                                <!-- <h2>what THEY SAY?</h2> -->
                            </div>
                            <div class="col-md-8 offset-md-2">
                                <div class="testimonials-section">
                                    <div class="testimonials-slider owl-carousel">
                                        <div class="item animate" data-animate="fadeInUp">
                                            <div class="testimonial-item">
                                                <div class="client-row">
                                                    <img src="{{ asset('assets/images/melawai.png') }}" class="rounded-circle" alt="profile 1">
                                                </div>
                                                <div class="testimonial-content">
                                                    <h4>Optik Melawai</h4>
                                                    <p>As an employee at the company PT. Inti Citra Agung (Optik Melawai), as a Fullstack Web Developer I was assigned to handle internal company projects such as HRIS, ESS, etc.</p>
                                                    <span>IT Developer</span>
                                                    <p>2021 Sep - 2022 Mar</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item animate" data-animate="fadeInUp">
                                            <div class="testimonial-item">
                                                <div class="client-row">
                                                    <img src="{{ asset('assets/images/satkomindo.png') }}" class="rounded-circle" alt="profile 2">
                                                </div>
                                                <div class="testimonial-content">
                                                    <h4>Satkomindo</h4>
                                                    <p>As an employee of a subsidiary of PT. Bank Rakyat Indonesia (Persero) Tbk, I was assigned to a project called BRISPOT as a Backend Developer.</p>
                                                    <span>Junior IT - Project Officer 2</span>
                                                    <p>2020 Sep - 2021 Aug</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item animate" data-animate="fadeInUp">
                                            <div class="testimonial-item">
                                                <div class="client-row">
                                                    <img src="{{ asset('assets/images/mii.png') }}" class="rounded-circle" alt="profile 3">
                                                </div>
                                                <div class="testimonial-content">
                                                    <h4>Mitra Integrasi Informatika</h4>
                                                    <p>As an employee of PT. Mitra Integrasi Informatika (Metrodata Group), I was assigned to PT. Bank Rakyat Indonesia (Persero) Tbk as a Fullstack Web Developer in a project called BRISPOT.</p>
                                                    <span>Associate Application Developer</span>
                                                    <p>2018 Sep - 2020 Aug</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item animate" data-animate="fadeInUp">
                                            <div class="testimonial-item">
                                                <div class="client-row">
                                                    <img src="{{ asset('assets/images/nts.png') }}" class="rounded-circle" alt="profile 3">
                                                </div>
                                                <div class="testimonial-content">
                                                    <h4>Nusa Tech Studio</h4>
                                                    <p>As an employee at a startup working on government projects, I was assigned as a Fullstack Web Developer.</p>
                                                    <span>Web Developer</span>
                                                    <p>2016 Jun - 2018 Jul</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section animated-row" data-section="slide04">
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-7 wide-col-laptop">
                            <div class="title-block animate" data-animate="fadeInUp">
                                <span>My Skills</span>
                            </div>
                            <div class="skills-row animate" data-animate="fadeInDown">
                                <div class="row">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="skill-item">
                                            <h6>PHP</h6>
                                            <div class="skill-bar">
                                                <span>90%</span>
                                                <div class="filled-bar-3"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item">
                                            <h6>Javascript</h6>
                                            <div class="skill-bar">
                                                <span>70%</span>
                                                <div class="filled-bar"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item">
                                            <h6>HTML</h6>
                                            <div class="skill-bar">
                                                <span>80%</span>
                                                <div class="filled-bar-2"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item">
                                            <h6>CSS</h6>
                                            <div class="skill-bar">
                                                <span>70%</span>
                                                <div class="filled-bar"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>MySQL</h6>
                                            <div class="skill-bar">
                                                <span>80%</span>
                                                <div class="filled-bar-2"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>PostgreSQL</h6>
                                            <div class="skill-bar">
                                                <span>70%</span>
                                                <div class="filled-bar"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>Git</h6>
                                            <div class="skill-bar">
                                                <span>80%</span>
                                                <div class="filled-bar-2"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>Laravel</h6>
                                            <div class="skill-bar">
                                                <span>80%</span>
                                                <div class="filled-bar-2"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>Codeigniter</h6>
                                            <div class="skill-bar">
                                                <span>90%</span>
                                                <div class="filled-bar-3"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>Bootstrap</h6>
                                            <div class="skill-bar">
                                                <span>70%</span>
                                                <div class="filled-bar"></div>
                                            </div>
                                        </div>
                                        <div class="skill-item last-skill">
                                            <h6>Linux</h6>
                                            <div class="skill-bar">
                                                <span>90%</span>
                                                <div class="filled-bar-3"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section animated-row" data-section="slide05">
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-8 wide-col-laptop">
                            <div class="title-block animate" data-animate="fadeInUp">
                                <span>My Development Experiences</span>
                            </div>
                            <div class="gallery-section">
                                <div class="gallery-list owl-carousel">
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-1.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>Psikotest - Optik Melawai</h4>
                                                <p>
                                                    Aplikasi website psikotest online. Aplikasi ini memiliki fitur mengatur pertanyaan, mengatur jadwal psikotest untuk kandidat, melihat hasil psikotest kandidat dan sekaligus untuk melakukan psikotest online.
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-2.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>Competitive Catalog - LKPP</h4>
                                                <p>
                                                    E-Katalog yang hanya bisa digunakan untuk produk dalam pengadaan barang/jasa, membuat LKPP untuk terus berinovasi dan menciptakan aplikasi atau sistem katalog yang mampu mendukung tender pekerjaan konstruksi.
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-6.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>PKP-SPM DIKDAS - KEMENDIKBUD</h4>
                                                <p>
                                                    Company Profile PKP-SPM Dikdas, Program Peningkatan Kapasitas Penerapan Standar Pelayanan Minimal Pendidikan Dasar Kemendikbud.
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-4.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>BRISPOT MAPS - Bank Rakyat Indonesia</h4>
                                                <p>
                                                    Aplikasi Internal tenaga pemasar BRI guna mempercepat proses prakarsa kredit dan efisiensi dalam memasarkan produk pinjaman BRI.
                                                    Fitur ini memudahkan tenaga pemasar untuk mengetahui letak asset maupun nasabah menggunakan google maps API.
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-3.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>Company Profile - Article 33</h4>
                                                <p>
                                                    Company Profile Article 33 Indonesia, Article 33 Indonesia adalah lembaga riset untuk perubahan sosial.
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item animate" data-animate="fadeInUp">
                                        <div class="portfolio-item">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/project-7.png') }}" alt="">
                                            </div>
                                            <div class="thumb-inner animate" data-animate="fadeInUp">
                                                <h4>Employee - Optik Melawai</h4>
                                                <p>
                                                    Aplikasi website untuk input data pribadi pekerja suatu perusahaan. Terdiri dari 2 role (Pekerja dan Admin).
                                                </p>
                                                <br />
                                                <a style="color:black;" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section animated-row" data-section="slide06">
                <div class="section-inner">
                    <div class="row justify-content-center">
                        <div class="col-md-7 wide-col-laptop">
                            <div class="title-block animate" data-animate="fadeInUp">
                                <span>Contact</span>
                            </div>
                            <div class="contact-section">
                                <div class="row">
                                    <div class="col-md-6 animate" data-animate="fadeInUp">
                                        <div class="contact-box">
                                            <div class="contact-row">
                                                <i class="fa fa-map-marker"></i> Depok, West Java, Indonesia
                                            </div>
                                            <div class="contact-row">
                                                <i class="fa fa-phone"></i> 085840102335
                                            </div>
                                            <div class="contact-row">
                                                <i class="fa fa-envelope"></i> vidya.wijaya92@gmail.com
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 animate" data-animate="fadeInUp">
                                        <form id="ajax-contact" method="post" action="#">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="name" id="name" required placeholder="Name">
                                            </div>
                                            <div class="input-field">
                                                <input type="email" class="form-control" name="email" id="email" required placeholder="Email">
                                            </div>
                                            <div class="input-field">
                                                <textarea class="form-control" name="message" id="message" required placeholder="Message"></textarea>
                                            </div>
                                            <button class="btn" type="submit">Submit</button>
                                        </form>
                                        <div id="form-messages" class="mt-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="social-icons">
            <div class="text-right">
                <ul class="social-icons">
                    <!-- <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li> -->
                    <!-- <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li> -->
                    <li><a href="https://www.linkedin.com/in/vidiswijaya-1992/" title="Linkedin" target="blank"><i class="fa fa-linkedin"></i></a></li>
                    <!-- <li><a href="#" title="Instagram"><i class="fa fa-behance"></i></a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/fullpage.min.js') }}"></script>
    <script src="{{ asset('assets/js/scrolloverflow.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>
    <script src="{{ asset('assets/js/form.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>